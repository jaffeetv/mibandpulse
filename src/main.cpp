#include <assert.h>
#include <cstdlib>
#include <iostream>
#include <string>
#include <glib-2.0/glib.h>


#include "gattlib.h"

void notification_handler(const uuid_t* uuid, const uint8_t* data, size_t data_length, void* user_data) {
	int i;

	printf("Notification Handler: ");

	for (i = 0; i < data_length; i++) {
		printf("%02x ", data[i]);
	}
	printf("\n");
}

int main(int argc, char *argv[]) {
  gatt_connection_t *conn = gattlib_connect(NULL, "C8:0F:10:3B:14:64", BDADDR_LE_PUBLIC, BT_SEC_LOW, 0, 0);
  gattlib_register_notification(conn, notification_handler, NULL);
  uint8_t buffer[100];

  if(conn == NULL) {
    std::cerr << "Fail to connect to ble device\n";
    return -1;
  }

  uint16_t data = strtol("0x1", NULL, 16);
  std::cout << "data: " << data << std::endl;
  
  uuid_t g_uuid;
  uuid_t pulse_uuid;
  std::string str_alert_uid = "00002a06-0000-1000-8000-00805f9b34fb";
  std::string str_pulsenot_uid = "00002902-0000-1000-8000-00805f9b34fb";
  gattlib_string_to_uuid(str_alert_uid.c_str(),str_alert_uid.length(), &g_uuid);
  gattlib_string_to_uuid(str_pulsenot_uid.c_str(), str_pulsenot_uid.length()+1, &pulse_uuid);

  if(gattlib_write_char_by_uuid(conn, &g_uuid, &data, sizeof(data)) != 0) {
    std::cerr << "write_char_by_handle return nut null!\n";
    return -1;
  }
  
  int ret = 0;
  if((ret = gattlib_notification_start(conn, &pulse_uuid)) != 0) {
    std::cerr << "notification_start return nut null: " << ret << ";\n";
    return -1;
  }
  
  uint32_t pulse_data = strtol("0x150201", NULL, 16);
  std::cout << "data: " << pulse_data << std::endl;
  if(gattlib_write_char_by_uuid(conn, &pulse_uuid, &pulse_data, sizeof(pulse_data)) != 0) {
      std::cerr << "pulse notify return nut null!\n";
      return -1;
  }
  
  GMainLoop *loop = g_main_loop_new(NULL, 0);
  g_main_loop_run(loop);
  
  g_main_loop_unref(loop);

  gattlib_disconnect(conn);
  std::cout << "DONE\n";
	return 0;
}
