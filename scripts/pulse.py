import sys, pexpect
from time import sleep
gatt=pexpect.spawn('gatttool -I')
gatt.logfile = open("pylog.txt", "w")
gatt.sendline('connect C8:0F:10:3B:14:64')
gatt.expect('Connection successful')
# Check battery level
gatt.sendline('char-read-hnd 0x002c')
gatt.expect('Characteristic value.*')
batt = gatt.after
batt = int(batt.split()[2],16)
print 'Battery level:', batt, '%'
# Send alert
gatt.sendline('char-write-cmd 0x0051 01')
sleep(5)
# Allow notification
gatt.sendline('char-write-req 0x004c 0100')
gatt.expect('Characteristic value.*')
# Send user data
gatt.sendline('char-write-req 0x0019 F8663A5F0126B45500040049676F7200000000DC')
gatt.expect('Characteristic value.*')
# Set control point
gatt.sendline('char-write-req 0x004e 150201')
# Waiting for notification
try:
    gatt.expect('Notification handle.*')
    hrm = gatt.after
    hrm = int(hrm.split()[6], 16)
    print 'HRM:', hrm
except:
    print 'Bad control point or timeout'
sys.exit(0)
